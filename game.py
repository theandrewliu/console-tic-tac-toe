# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board

def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

board=[1, 2, 3, 4, 5, 6, 7, 8, 9]
player = "X"

for turn in board:  #each turn 
    print_board(board) 

    print("Player ", player, ",")
    move = input("where would you like to move?\n")  #X or O chooses a spot on the board
    move = int(move)
    
    board[move-1]= player

    if board[0] == board[1] and board[1] == board[2]:       
        print_board(board)
        print(board[0], "has won! Congrats!")
        exit()
    if board[3] == board[4] and board[4] == board[5]:       
        print_board(board)
        print(board[3], "has won! Congrats!")
        exit()
    if board[6] == board[7] and board[7] == board[8]:       
        print_board(board)
        print(board[6], "has won! Congrats!")
        exit()
    if board[0] == board[3] and board[3] == board[6]:       
        print_board(board)
        print(board[0], "has won! Congrats!")
        exit()
    if board[1] == board[4] and board[4] == board[7]:       
        print_board(board)
        print(board[1], "has won! Congrats!")
        exit()
    if board[2] == board[5] and board[5] == board[8]:       
        print_board(board)
        print(board[2], "has won! Congrats!")
        exit()
    if board[0] == board[4] and board[4] == board[8]:       
        print_board(board)
        print(board[0], "has won! Congrats!")
        exit()       
    if board[2] == board[4] and board[4] == board[6]:       
        print_board(board)
        print(board[2], "has won! Congrats!")
        exit()

    if player == "X":
        player = "O"
    elif player == "O":
        player = "X"